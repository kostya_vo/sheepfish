from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404

from .dbconnector.categories import check_post_by_category
from .dbconnector.coment_form import save_comment_form
from .dbconnector.contact_form import save_contact_form
from .dbconnector.paginator import get_paginator
from .dbconnector.post import get_posts_detail
from .dbconnector.redis import check_posts_by_redis, check_redis_settings
from .models import Post, Carousel, Category


def post_list(request):
    object_list = Post.published.all()
    categories = Category.objects.all()
    carousel = Carousel.objects.order_by('index').all()
    contact_form = save_contact_form(request)

    paginator = Paginator(object_list, 10)  # По 3 статьи на каждой странице.
    page = request.GET.get('page')
    posts = get_paginator(page, paginator)

    r = check_redis_settings()
    post_ranking = r.zrange('post', 0, -1, desc=False)[:10]
    posts_ranking_ids = [int(id) for id in post_ranking]
    # Получаем отсортированный список самых популярных постов.
    most_viewed = list(Post.objects.filter(id__in=posts_ranking_ids))
    most_viewed.sort(key=lambda x: posts_ranking_ids.index(x.id))
    most_viewed = list(Post.objects.filter(id__in=posts_ranking_ids))

    return render(request, 'blog/post/list.html', {'page': page,
                                                   'posts': posts,
                                                   'categories': categories,
                                                   'most_viewed': most_viewed,
                                                   'contact_form': contact_form,
                                                   'carousel': carousel})


def post_detail(request, year, month, day, post):
    post = get_posts_detail(year, month, day, post)
    total_views = check_posts_by_redis(post)
    # Список активных комментариев для этой статьи.
    comments = post.comments.filter(active=True)

    comment_form = save_comment_form(request, post)
    new_comment = None
    return render(request, 'blog/post/detail.html', {'post': post,
                                                     'comments': comments,
                                                     'new_comment': new_comment,
                                                     'comment_form': comment_form,
                                                     'total_views': total_views})


def category_detail(request, category):
    post_list_by_category = check_post_by_category(category)
    category_by_name = Category.objects.filter(name__iexact=category)
    paginator = Paginator(post_list_by_category, 5)
    page = request.GET.get('page')
    posts = get_paginator(page, paginator)
    return render(request, 'blog/post/category_detail.html', {'post_list_by_category': post_list_by_category,
                                                              'category': category,
                                                              'category_by_name': category_by_name,
                                                              'page': page,
                                                              'posts': posts})

