# Generated by Django 2.2.16 on 2020-09-23 15:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20200923_1103'),
    ]

    operations = [
        migrations.AddField(
            model_name='carousel',
            name='image',
            field=models.ImageField(blank=True, upload_to='carousel/%Y/%m/%d'),
        ),
    ]
