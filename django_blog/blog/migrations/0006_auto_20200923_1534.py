# Generated by Django 2.2.16 on 2020-09-23 15:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_carousel_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='carousel',
            name='id',
        ),
        migrations.AddField(
            model_name='carousel',
            name='index',
            field=models.IntegerField(auto_created=True, default=1, primary_key=True, serialize=False),
        ),
    ]
