import redis

from django_blog import settings


def check_redis_settings():
    return redis.StrictRedis(host=settings.REDIS_HOST,
                             port=settings.REDIS_PORT,
                             db=settings.REDIS_DB)


def check_posts_by_redis(post):
    r = check_redis_settings()
    # Увеличиваем количество просмотров поста на 1.
    total_views = r.incr('post:{}:views'.format(post.id))
    # Увеличиваем рейтинг поста на 1.
    r.zincrby('post', post.id, 1)
    return total_views


def check_all_posts(*args, **kwargs):
    r = check_redis_settings()
    # Увеличиваем количество просмотров поста на 1.
    total = r.getset('views', **kwargs)