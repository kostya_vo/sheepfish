from django.shortcuts import redirect

from blog.forms import ContactForm


def save_contact_form(request):
    if request.method == 'POST':
        # Пользователь отправил комментарий.
        contact_form = ContactForm(data=request.POST)
        if contact_form.is_valid():
            # Создаем комментарий, но пока не сохраняем в базе данных.
            new_comment = contact_form.save(commit=False)
            # Сохраняем комментарий в базе данных.
            new_comment.save()
            return redirect('/blog/')
    else:
        contact_form = ContactForm()
    return contact_form
