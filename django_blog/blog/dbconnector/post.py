from django.shortcuts import get_object_or_404

from blog.models import Post


def get_posts_detail(year, month, day, post):
    return get_object_or_404(Post,
                             slug=post,
                             status='published',
                             publish__year=year,
                             publish__month=month,
                             publish__day=day)