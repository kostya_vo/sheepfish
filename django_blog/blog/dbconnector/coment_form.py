from django.http import HttpResponseRedirect

from blog.forms import CommentForm


def save_comment_form(request, post):
    if request.method == 'POST':
        # Пользователь отправил комментарий.
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Создаем комментарий, но пока не сохраняем в базе данных.
            new_comment = comment_form.save(commit=False)
            # Привязываем комментарий к текущей статье.
            new_comment.post = post
            # Сохраняем комментарий в базе данных.
            new_comment.save()
            comment_form = CommentForm()
    else:
        comment_form = CommentForm()
    return comment_form
