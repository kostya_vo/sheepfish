from blog.models import Post


def check_post_by_category(category):
    post_list_by_category = Post.objects.filter(category__slug=category)
    return post_list_by_category
