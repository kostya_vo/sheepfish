CD my_work directory

update/upgrade/install basic packages from django_blog folder:

- sudo apt update
- sudo apt upgrade
- sudo apt-get install python3-dev libpq-dev build-essential python3.6 python3.6-venv

create virtualenv:
- python -m venv env

Activate environment:
- . env/bin/activate

upgrade pip:
- pip install --upgrade pip

install requirements.txt:
- pip install -r requirements.txt

install redis for comments django_blog
-sudo apt install redis-server


start redis server:
- cd redis/redis-6.0.8/
- redis-server

start django_blog:
- python manage.py runserver
